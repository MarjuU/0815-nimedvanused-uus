﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0815_nimedvanused_uus
{
    class Program
    {
        static void Main(string[] args)
        {
            //loo õpilaste nimede ja vanuste jaoks sõnastik
            Dictionary<string, int> opilane = new Dictionary<string, int>();

            //küsi kasutajalt nimesid ja vanuseid
            while(true)
            {
                Console.WriteLine("Sisesta nimi:");
                string nimi = Console.ReadLine();
                if (nimi == "") break;
                Console.WriteLine("Sisesta vanus: ");
                int vanus = int.Parse(Console.ReadLine());
                opilane.Add(nimi, vanus);
            }

            Console.WriteLine($"Õpilasi sisestatud: {opilane.Count}");

            // prindib iga õpilase vanuse
            foreach (var x in opilane) Console.WriteLine($"nimi: {x.Key}, vanus: {x.Value}");

            //leida keskmine vanus. seda pole vaja pikalt teha, sest average toimib, sest pole null-väärtusi
            double keskmine = opilane.Values.Average();

            //double summa = 0;
            //for (int i = 0; i < opilane.Count; i++)
            //{
            //    summa += opilane.ElementAt(i).Value;
            //}

            //double keskmine2 = summa / opilane.Count;
            Console.WriteLine($"õpilaste keskmine vanus on {keskmine}");

            // leia, kelle vanus on keskmisele kõige lähemal
            double vahimKaugusKeskmisest = keskmine;
            string lahim = "";

            foreach (var x in opilane)
            {
                double kaugusKeskmisest = Math.Abs(x.Value - keskmine);
                if (vahimKaugusKeskmisest > kaugusKeskmisest)
                {
                    vahimKaugusKeskmisest = kaugusKeskmisest;
                    lahim = x.Key;
                }

            }

            Console.WriteLine($"keskmisele vanusele lähim on: {lahim}");












        }
    }
}
